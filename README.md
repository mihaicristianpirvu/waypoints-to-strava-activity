## Waypoints to Strava Activity

Steps:

1. Use this tool: https://www.gpxgenerator.com/ to generate your data.

2. Then use `python main.py current/gps_web.gpx /path/to/output.gpx startDate endDate`
where `startDate` and `endDate` are in YYYYMMDDhhmmss format. Example: 20201012221300 (for 2020-10-12 22:13:00)

3. After that, upload the activity manually to strava using the website. Use `race` as activity type when prompted.

4. Eternal gratitude

It is recommended that you use many waypoints in the web tool above, otherwise Strava might skip them and end your run prematurely.
TODO: Add --numWaypointsPerMeter=N to linearly interpolate waypoints based on meters. 

