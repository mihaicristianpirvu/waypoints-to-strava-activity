import pandas as pd
import xmltodict
import numpy as np
from geopy.distance import distance
from datetime import datetime
from argparse import ArgumentParser
from collections import OrderedDict
np.set_printoptions(precision=3, suppress=True)

def getArgs():
    parser = ArgumentParser()
    parser.add_argument("inputGPX")
    parser.add_argument("outputGPX")
    parser.add_argument("startDate")
    parser.add_argument("endDate")
    parser.add_argument("--numWaypointsPerMeter", type=int, default=1)
    parser.add_argument("--name", default="Random run")
    parser.add_argument("--type", default="run")
    args = parser.parse_args()
    args.startDate = pd.to_datetime(args.startDate)
    args.endDate = pd.to_datetime(args.endDate)
    args.type = {
        "run" : 9
    }[args.type]

    return args

def datetimeToStr(Date):
    Date = str(Date)[0 : 19]
    res = str(datetime.strptime(Date, "%Y-%m-%d %H:%M:%S"))
    res = "%sT%sZ" % (res[0 : 10], res[11 :])
    return res

def normalizeDistances(lat, lon, ele, timestamps, numWaypointsPerMeter):
    latLon = [(_lat, _lon) for (_lat, _lon) in zip(lat, lon)]
    meters = [distance(a, b).m for (a, b) in zip(latLon[0 : -1], latLon[1 :])]
    # TODO
    return lat, lon, ele, timestamps

def getActivity(lat, lon, ele, timestamps):
    activity = [
        {
            "@lat" : _lat,
            "@lon" : _lon,
            "ele" : _ele,
            "time" : _timestamp
        } for (_lat, _lon, _ele, _timestamp) in zip(lat, lon, ele, timestamps)
    ]
    return activity

def normalizeTimes(lat, lon, ele, startDate, endDate, numWaypointsPerMeter):
    assert len(lat) == len(lon)
    assert len(lat) == len(ele)

    latDiff = lat[1 : ] - lat[0 : -1]
    lonDiff = lon[1 : ] - lon[0 : -1]
    Diffs = latDiff**2 + lonDiff**2
    Diffs = np.cumsum(Diffs / Diffs.sum())

    X = (endDate - startDate) * Diffs
    Y = [startDate + x for x in X]
    Range = [startDate, *Y]
    timestamps = list(map(lambda x : datetimeToStr(x), Range))
    assert len(lat) == len(timestamps)

    lat, lon, ele, timestamps = normalizeDistances(lat, lon, ele, timestamps, numWaypointsPerMeter)
    activity = getActivity(lat, lon, ele, timestamps)
    return activity

def writeActivity(activity, outputFile, name, type, startDate):
    gpx = OrderedDict({
        "gpx" : 
        {
            "@creator" : "StravaGPX Android",
            "@xmlns:xsi" : "http://www.w3.org/2001/XMLSchema-instance",
            "@xsi:schemaLocation" : "http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd",
            "@version" : "1.1",
            "@xmlns" : "http://www.topografix.com/GPX/1/1",
            "@metadata" : {"time" : datetimeToStr(startDate)},
            "trk" : {
                "name" : name,
                "type" : type,
                "trkseg" : {"trkpt" : activity}
            }
        }
    })

    f = open(outputFile, "w")
    f.write(xmltodict.unparse(gpx, pretty=True))
    print("Written to %s" % outputFile)

def main():
    args = getArgs()
    
    wpts = xmltodict.parse(open(args.inputGPX, "r").read())["gpx"]["wpt"]
    lat = np.array([float(wpt["@lat"]) for wpt in wpts])
    lon = np.array([float(wpt["@lon"]) for wpt in wpts])
    ele = np.array([float(wpt["ele"]) for wpt in wpts])

    activity = normalizeTimes(lat, lon, ele, args.startDate, args.endDate, args.numWaypointsPerMeter)
    writeActivity(activity, args.outputGPX, args.name, args.type, args.startDate)

if __name__ == "__main__":
    main()

